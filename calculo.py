"""Modulo para calcular a nota do Grau A e Grau B."""


class Grau:
    """Classe para calcular a nota final."""

    def __init__(self, grauA, grauB):
        """Construtor da classe."""
        self.grauA = grauA
        self.grauB = grauB

    def calculo_final(self):
        u"""Método para calcular a nota final."""
        return (self.grauA + (self.grauB * 2)) / 3
